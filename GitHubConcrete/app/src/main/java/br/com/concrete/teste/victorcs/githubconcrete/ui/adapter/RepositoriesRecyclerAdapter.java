package br.com.concrete.teste.victorcs.githubconcrete.ui.adapter;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import br.com.concrete.teste.victorcs.githubconcrete.R;
import br.com.concrete.teste.victorcs.githubconcrete.logic.model.Items;
import br.com.concrete.teste.victorcs.githubconcrete.logic.model.Owner;
import br.com.concrete.teste.victorcs.githubconcrete.ui.view.ProgressViewHolder;
import br.com.concrete.teste.victorcs.githubconcrete.ui.view.RepositoryViewHolder;

/**
 * Created by Victor Santiago on 20/09/2017.
 */
public class RepositoriesRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final Context context;
    private List<Items> items;

    private final int VIEW_ITEM = 0;
    private final int TYPE_LOAD = 1;
    private OnLoadMoreListener onLoadMoreListener;
    private boolean loading;
    private boolean isMoreDataAvailable = true;

    private int visibleThreshold = 2;
    private int lastVisibleItem, totalItemCount;

    public RepositoriesRecyclerAdapter(Context context, List<Items> items, RecyclerView recyclerView) {
        this.context = context;
        this.items = items;

        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {

            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);

                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        // End has been reached
                        // Do something
                        if (onLoadMoreListener != null) {
                            onLoadMoreListener.onLoadMore();
                        }
                        loading = true;
                    }
                }
            });
        }
    }

    public void addMoreItems(Items[] newItems) {

        for (int i = 0; i < newItems.length - 1; i++) {

            items.add(newItems[i]);
            notifyDataChanged();

        }

        items.add(null);
        notifyDataChanged();
        setLoaded();

    }

    @Override
    public int getItemViewType(int position) {
        return items.get(position) != null ? VIEW_ITEM : TYPE_LOAD;
    }

    public Items getListItemByPosition(int index) {

        return items.get(index);

    }

    public void removeItemByPosition(int index) {

        items.remove(index);

    }

    public void setLoaded() {
        loading = false;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                      int viewType) {

        if (viewType == VIEW_ITEM) {

            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.recyclerview_item_repository, parent, false);
            return new RepositoryViewHolder(v, context);

        }

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.progress_item, parent, false);
        return new ProgressViewHolder(v);

    }


    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        if (position >= getItemCount() - 1 && isMoreDataAvailable && !loading && onLoadMoreListener != null) {
            loading = true;
            onLoadMoreListener.onLoadMore();
        }

//        if (holder instanceof RepositoryViewHolder) {
        if (getItemViewType(position) == VIEW_ITEM) {

            Items myListItem = items.get(position);
            Owner owner = myListItem.getOwner();
            if (owner != null) {

                ((RepositoryViewHolder) holder).setUserPhoto(myListItem.getOwner().getAvatarUrl());
                ((RepositoryViewHolder) holder).setUsername(myListItem.getOwner().getLogin());

            }
            ((RepositoryViewHolder) holder).setRepositoryName(myListItem.getName());
            ((RepositoryViewHolder) holder).setRepositoryDescription(myListItem.getDescription());
            ((RepositoryViewHolder) holder).setNumFork(myListItem.getForksCount());
            ((RepositoryViewHolder) holder).setNumStar(myListItem.getStargazersCount());

//            holder.setIsRecyclable(false);

        } else {

            loading = true;
            onLoadMoreListener.onLoadMore();

        }

    }

    @Override
    public int getItemCount() {
        if (items == null) {
            return 0;
        }
        return items.size();
    }

    public void removeAllItems() {

        items = new ArrayList<>();
        notifyDataSetChanged();

    }

    public void setMoreDataAvailable(boolean moreDataAvailable) {
        isMoreDataAvailable = moreDataAvailable;
    }

    public void notifyDataChanged() {
        notifyDataSetChanged();
        loading = false;
    }

    public List<Items> getItems() {
        return items;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    public interface OnLoadMoreListener {
        void onLoadMore();
    }

}