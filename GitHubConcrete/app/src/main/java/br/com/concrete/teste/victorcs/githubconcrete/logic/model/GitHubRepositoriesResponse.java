package br.com.concrete.teste.victorcs.githubconcrete.logic.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Victor Santiago on 20/09/2017.
 */
public class GitHubRepositoriesResponse {

    @SerializedName("incomplete_results")
    private String incompleteResults;

    @SerializedName("items")
    private Items[] items;

    @SerializedName("total_count")
    private String totalCount;

    public String getIncompleteResults() {
        return incompleteResults;
    }

    public void setIncompleteResults(String incompleteResults) {
        this.incompleteResults = incompleteResults;
    }

    public Items[] getItems() {
        return items;
    }

    public void setItems(Items[] items) {
        this.items = items;
    }

    public String getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(String totalCount) {
        this.totalCount = totalCount;
    }

}
