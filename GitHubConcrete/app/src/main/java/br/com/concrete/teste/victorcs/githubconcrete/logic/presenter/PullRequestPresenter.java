package br.com.concrete.teste.victorcs.githubconcrete.logic.presenter;

import android.content.Intent;

import java.util.ArrayList;
import java.util.List;

import br.com.concrete.teste.victorcs.githubconcrete.application.AppConstants;
import br.com.concrete.teste.victorcs.githubconcrete.logic.model.PullRequestItem;
import br.com.concrete.teste.victorcs.githubconcrete.ui.view.IBaseView;

/**
 * Created by VictorCS on 20/09/2017.
 */
public class PullRequestPresenter implements IBasePresenter {

    private IPullRequestView view;
    private ArrayList<PullRequestItem> pullRequestItem;
    private String title;

    public PullRequestPresenter(IPullRequestView view, Intent intent) {

        this.view = view;
        this.title = intent.getStringExtra(AppConstants.PULL_REQUEST_TITLE);
        this.pullRequestItem = intent.getParcelableArrayListExtra(AppConstants.PARCE_PULL_REQUEST);
        this.view.setPresenter(this);

    }

    @Override
    public void start() {

        view.setupToolbar(title);
        if (pullRequestItem != null &&
                pullRequestItem.size() > 0) {

            view.setupValues(pullRequestItem);
            view.setupRecyclerView(pullRequestItem);

        } else {

            view.showEmptyLayout();

        }
    }

    public interface IPullRequestView extends IBaseView {

        void setupToolbar(String title);

        void setPresenter(PullRequestPresenter presenter);

        void setupValues(List<PullRequestItem> items);

        void setupRecyclerView(List<PullRequestItem> items);

        void showEmptyLayout();

        void callBrowserActivity();

    }

}
