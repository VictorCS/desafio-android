package br.com.concrete.teste.victorcs.githubconcrete.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

/**
 * Created by VictorCS on 20/09/2017.
 */
public abstract class AppUtils {

    /**
     * Open URL in browser
     *
     * @param context - current context
     * @param url     - url to open
     */
    public static void openUrl(Context context, String url) {

        if (!url.contains("http://") && !url.contains("https://")) {

            url = "http://" + url;

        }

        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        context.startActivity(i);
    }

}
