package br.com.concrete.teste.victorcs.githubconcrete.ui.view;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import br.com.concrete.teste.victorcs.githubconcrete.R;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Victor Santiago on 20/09/2017.
 */
public class RepositoryViewHolder extends RecyclerView.ViewHolder {

    private Context context;
    @BindView(R.id.tvRepositoryName)
    protected TextView tvRepositoryName;
    @BindView(R.id.tvRepositoryDescription)
    protected TextView tvRepositoryDescription;
    @BindView(R.id.llFork)
    protected LinearLayout llFork;
    @BindView(R.id.tvRepositoryFork)
    protected TextView tvRepositoryFork;
    @BindView(R.id.llStar)
    protected LinearLayout llStar;
    @BindView(R.id.tvRepositoryStar)
    protected TextView tvRepositoryStar;
    @BindView(R.id.llUser)
    protected LinearLayout llUser;
    @BindView(R.id.ivPostProfilePhoto)
    protected CircleImageView ivPostProfilePhoto;
    @BindView(R.id.tvUserUsername)
    protected TextView tvUserUsername;

    public RepositoryViewHolder(final View itemView, Context context) {

        super(itemView);
        ButterKnife.bind(this, itemView);
        this.context = context;

    }

    public void setRepositoryName(String name) {

        name = TextUtils.isEmpty(name) ? "" : name;
        tvRepositoryName.setText(name);

    }

    public void setRepositoryDescription(String description) {

        description = TextUtils.isEmpty(description) ? "" : description;
        tvRepositoryDescription.setText(description);

    }

    public void setNumFork(String num) {

        tvRepositoryFork.setText(num);

    }


    public void setNumStar(String num) {

        tvRepositoryStar.setText(num);

    }

    public void setUserPhoto(String url) {

        Glide.with(context).load(url)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .error(ContextCompat.getDrawable(context, R.drawable.ic_default_user))
                .into(ivPostProfilePhoto);

    }

    public void setUsername(String username) {

        username = TextUtils.isEmpty(username) ? "" : username;
        tvUserUsername.setText(username);

    }

}