package br.com.concrete.teste.victorcs.githubconcrete.logic.presenter;

/**
 * Created by Victor Santiago on 20/09/2017.
 */

public interface IBasePresenter {

    void start();

}
