package br.com.concrete.teste.victorcs.githubconcrete.logic.rest;

import java.util.ArrayList;
import java.util.List;

import br.com.concrete.teste.victorcs.githubconcrete.logic.model.GitHubRepositoriesResponse;
import br.com.concrete.teste.victorcs.githubconcrete.logic.model.PullRequestItem;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Victor Santiago on 20/09/2017.
 */
public interface AppApi {

    @GET("/search/repositories?q=language:Java&sort=stars")
    Observable<GitHubRepositoriesResponse> getRepoLists(@Query("page") int pageNum);

    @GET("/repos/{owner_login}/{item_name}/pulls")
    Observable<ArrayList<PullRequestItem>> getPRFromSelection(@Path("owner_login") String ownerLogin,
                                                              @Path("item_name") String itemName);

}
