package br.com.concrete.teste.victorcs.githubconcrete.application;

/**
 * Created by Victor Santiago on 20/09/2017.
 */
public abstract class AppConstants {

    /**
     * TEST
     */
    public static final boolean DEV_MODE = false;

    /**
     * Reqres.in - A hosted sample REST-API
     */
    public static final String API_GITHUB_URL = "https://api.github.com/";

    /**
     * Request custom header
     */
    public static final String APPLICATION_JSON = "application/json";
    public static final String ACCEPT = "Accept";
    public static final String CONTENT_TYPE = "Content-Type";
    public static final String APPLICATION_JSON_CHARSET_UTF_8 = "application/json; charset=utf-8";

    /**
     * Logs - Internal
     */
    public static final String ERROR = "-- ERROR --";
    public static final String INFO = "-- INFO --";

    /**
     * Send data - Internal
     */
    public static final String PULL_REQUEST_TITLE = "pull_request_title";
    public static final String PARCE_PULL_REQUEST = "parce_pull_request";
}
