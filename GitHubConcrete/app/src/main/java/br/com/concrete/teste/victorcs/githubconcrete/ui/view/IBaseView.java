package br.com.concrete.teste.victorcs.githubconcrete.ui.view;

import android.content.Context;

/**
 * Created by Victor Santiago on 20/09/2017.
 */

public interface IBaseView {

    void showProgress();

    void hideProgress();

    void showToastMessage(String message);

    void showSnackMessage(String message);

    void showAlerterMessage(String title, String message, int iconResId, int backgroundColor);

    void showActionErrorAlerterMessage();

    void showNoConnectionAlerterMessage();

    void showNoConnectionSnackMessage();

    boolean isOnline();

    void hideSoftKeyboard();

    void showDialogOkMessage(Context context, int idString);

}
