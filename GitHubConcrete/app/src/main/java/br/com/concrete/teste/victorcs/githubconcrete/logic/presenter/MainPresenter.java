package br.com.concrete.teste.victorcs.githubconcrete.logic.presenter;

import java.util.ArrayList;

import br.com.concrete.teste.victorcs.githubconcrete.R;
import br.com.concrete.teste.victorcs.githubconcrete.application.AppConstants;
import br.com.concrete.teste.victorcs.githubconcrete.logic.interactor.GitHubInteractor;
import br.com.concrete.teste.victorcs.githubconcrete.logic.listener.OnGitHubRepositoriesListener;
import br.com.concrete.teste.victorcs.githubconcrete.logic.listener.OnPullRequestListener;
import br.com.concrete.teste.victorcs.githubconcrete.logic.model.GitHubRepositoriesResponse;
import br.com.concrete.teste.victorcs.githubconcrete.logic.model.Items;
import br.com.concrete.teste.victorcs.githubconcrete.logic.model.PullRequestItem;
import br.com.concrete.teste.victorcs.githubconcrete.ui.view.IBaseView;
import br.com.concrete.teste.victorcs.githubconcrete.utils.TimberHelper;

/**
 * Created by VictorCS on 20/09/2017.
 */
public class MainPresenter implements IBasePresenter, OnGitHubRepositoriesListener, OnPullRequestListener {

    private GitHubInteractor interactor;
    private IMainView view;
    private String repositoryName;
    private boolean isFirstLoad;

    public MainPresenter(IMainView view) {

        this.view = view;
        this.interactor = new GitHubInteractor();
        this.isFirstLoad = true;
        this.view.setPresenter(this);

    }

    @Override
    public void start() {

        view.setupToolbar();
        view.setupDrawerMenu();
        attemptGetRepositories(1);
    }


    //region WebServices
    public String getRepositoryName() {
        return repositoryName;
    }

    public void setRepositoryName(String repositoryName) {
        this.repositoryName = repositoryName;
    }

    //endregion

    //region WebServices
    public void attemptGetRepositories(int numPage) {

        if (view.isOnline()) {

            if(isFirstLoad) {
                view.showProgress();
            }

            try {

                interactor.getRepoLists(numPage, this);

            } catch (Exception e) {

                TimberHelper.e(AppConstants.ERROR, "getRepoLists: " + e.toString());
                view.showActionErrorAlerterMessage();
                if(isFirstLoad) {
                    view.hideProgress();
                }

            }

        } else {

            view.showNoConnectionAlerterMessage();
            if(isFirstLoad) {
                view.hideProgress();
            }

        }

    }

    @Override
    public void onGitHubRepositoriesSuccess(GitHubRepositoriesResponse response) {

        if(isFirstLoad) {
            view.hideProgress();
        }
        if (response != null && response.getItems().length > 0) {

            if(isFirstLoad) {

                view.setupRecyclerView(response.getItems());
                isFirstLoad = false;

            } else {

                if(response.getItems().length > 0) {

                    view.fetchRecyclerViewData(response.getItems());

                } else {

                    view.showNoDataAlerterMessage();

                }
                isFirstLoad = false;

            }

        } else {

            view.showEmptyLayout();

        }

    }

    @Override
    public void onGitHubRepositoriesError(String error) {

        if(isFirstLoad) {
            view.hideProgress();
            view.showEmptyLayout();
            view.showActionErrorAlerterMessage();
        }
        TimberHelper.e("onGitHubRepositoriesError", error);

    }

    public void attemptGetPullRequest(String ownerLogin, String itemName) {

        if (view.isOnline()) {

            view.showProgress();

            try {

                interactor.getPRFromSelection(ownerLogin, itemName, this);

            } catch (Exception e) {

                TimberHelper.e(AppConstants.ERROR, "getPRFromSelection: " + e.toString());
                view.showActionErrorAlerterMessage();
                view.hideProgress();

            }

        } else {

            view.showNoConnectionAlerterMessage();
            view.hideProgress();

        }

    }

    @Override
    public void onPullRequestSuccess(ArrayList<PullRequestItem> response) {

        view.hideProgress();

        if(response != null) {

            view.callPullRequestActivity(response);

        } else {

            view.showActionErrorAlerterMessage();

        }

    }

    @Override
    public void onPullRequestError(String error) {

        view.hideProgress();
        view.showEmptyLayout();
        view.showActionErrorAlerterMessage();
        TimberHelper.e("onPullRequestError", error);

    }
    //endregion

    public interface IMainView extends IBaseView {

        void setupToolbar();

        void setupDrawerMenu();

        void setPresenter(MainPresenter presenter);

        void setupRecyclerView(Items[] items);

        void showEmptyLayout();

        void callPullRequestActivity(ArrayList<PullRequestItem> response);

        void fetchRecyclerViewData(Items[] items);

        void showNoDataAlerterMessage();

    }

}
