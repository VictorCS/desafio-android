package br.com.concrete.teste.victorcs.githubconcrete.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import br.com.concrete.teste.victorcs.githubconcrete.R;
import br.com.concrete.teste.victorcs.githubconcrete.application.AppConstants;
import br.com.concrete.teste.victorcs.githubconcrete.logic.listener.RecyclerItemClickListener;
import br.com.concrete.teste.victorcs.githubconcrete.logic.model.Items;
import br.com.concrete.teste.victorcs.githubconcrete.logic.model.PullRequestItem;
import br.com.concrete.teste.victorcs.githubconcrete.logic.presenter.MainPresenter;
import br.com.concrete.teste.victorcs.githubconcrete.ui.adapter.RepositoriesRecyclerAdapter;
import br.com.concrete.teste.victorcs.githubconcrete.utils.TimberHelper;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by VictorCS on 20/09/2017.
 */
public class MainActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener, MainPresenter.IMainView {

    private static boolean isCalledActivity = false;

    @BindView(R.id.toolbar)
    protected Toolbar toolbar;
    @BindView(R.id.drawer_layout)
    protected DrawerLayout drawer;
    @BindView(R.id.nav_view)
    protected NavigationView navigationView;
    @BindView(R.id.rvRepositories)
    protected RecyclerView rvRepositories;
    @BindView(R.id.ivEmpty)
    protected ImageView ivEmpty;
    @BindView(R.id.tvBoldEmpty)
    protected TextView tvBoldEmpty;
    @BindView(R.id.llEmptyResult)
    protected LinearLayout llEmptyResult;

    private RepositoriesRecyclerAdapter adapter;
    private MainPresenter mPresenter;
    private int currentPage = 1;
    private LinearLayoutManager layoutManager;

    //region Activity life cycle
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        isCalledActivity = false;
        new MainPresenter(this);

    }

    @Override
    public void onBackPressed() {

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }

    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_exit) {

            finish();

        }

        drawer.closeDrawer(GravityCompat.START);
        return true;

    }

    @Override
    protected void onRestart() {

        super.onRestart();
        isCalledActivity = false;

    }
    //endregion

    //region IMainView
    @Override
    public void setupToolbar() {

        setSupportActionBar(toolbar);

    }

    @Override
    public void setupDrawerMenu() {

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);

    }

    @Override
    public void setPresenter(MainPresenter presenter) {

        this.mPresenter = presenter;
        this.mPresenter.start();
        this.currentPage = 1;

    }

    @Override
    public void setupRecyclerView(Items[] items) {

        llEmptyResult.setVisibility(View.GONE);
        rvRepositories.setVisibility(View.VISIBLE);

        rvRepositories.setHasFixedSize(false);

        List<Items> asList = Arrays.asList(items);
        List<Items> asList2 = new ArrayList<>(asList);
        asList2.add(null);
        adapter = new RepositoriesRecyclerAdapter(this, asList2, rvRepositories);
        layoutManager = new LinearLayoutManager(getApplicationContext());
        rvRepositories.setLayoutManager(layoutManager);

        adapter.setOnLoadMoreListener(new RepositoriesRecyclerAdapter.OnLoadMoreListener() {
            @Override
            public void onLoadMore() {

                rvRepositories.post(new Runnable() {
                    @Override
                    public void run() {

                        mPresenter.attemptGetRepositories(++currentPage);

                    }
                });

            }
        });

        rvRepositories.setAdapter(adapter);
        rvRepositories.setNestedScrollingEnabled(false);
        rvRepositories.addOnItemTouchListener(new RecyclerItemClickListener(this,
                rvRepositories, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                if (!isCalledActivity) {

                    isCalledActivity = true;

                    try {

                        Items listItemByPosition = adapter.getListItemByPosition(position);
                        if (listItemByPosition != null &&
                                listItemByPosition.getOwner() != null) {

                            mPresenter.attemptGetPullRequest(listItemByPosition.getOwner().getLogin(),
                                    listItemByPosition.getName());
                            mPresenter.setRepositoryName(listItemByPosition.getName());

                        } else {

                            showActionErrorAlerterMessage();
                            TimberHelper.e("onItemClick", "Some item data is null!");

                        }

                    } catch (Exception e) {

                        showActionErrorAlerterMessage();
                        TimberHelper.e("onItemClick", "Some item data is null!");

                    }

                }

            }

            @Override
            public void onItemLongClick(View view, int position) {

            }
        }));

    }

    @Override
    public void fetchRecyclerViewData(Items[] items) {

        adapter.setMoreDataAvailable(false);
        adapter.removeItemByPosition(adapter.getItemCount() - 1);
        adapter.addMoreItems(items);

    }

    @Override
    public void showNoDataAlerterMessage() {

        adapter.getListItemByPosition(adapter.getItemCount() - 1);
        adapter.setMoreDataAvailable(false);
        showAlerterMessage(getString(R.string.warning), getString(R.string.no_data), R.drawable.ic_warning_white,
                R.color.blueApp);

    }

    @Override
    public void showEmptyLayout() {

        llEmptyResult.setVisibility(View.VISIBLE);
        rvRepositories.setVisibility(View.GONE);

    }

    @Override
    public void callPullRequestActivity(ArrayList<PullRequestItem> response) {

        Intent intent = new Intent(MainActivity.this, PullRequestActivity.class);
        intent.putExtra(AppConstants.PULL_REQUEST_TITLE, mPresenter.getRepositoryName());
        intent.putParcelableArrayListExtra(AppConstants.PARCE_PULL_REQUEST, response);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);

    }
    //endregion
}
