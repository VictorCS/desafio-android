package br.com.concrete.teste.victorcs.githubconcrete.logic.rest;

import java.io.IOException;

import br.com.concrete.teste.victorcs.githubconcrete.application.AppConstants;
import br.com.concrete.teste.victorcs.githubconcrete.utils.TimberHelper;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Victor Santiago on 20/09/2017.
 */
public class AppHeaderInterceptor implements Interceptor {

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request original = chain.request();

        // Request customization: add request headers
        Request.Builder requestBuilder = original.newBuilder()
                .addHeader(AppConstants.ACCEPT, AppConstants.APPLICATION_JSON)
                .addHeader(AppConstants.CONTENT_TYPE, AppConstants.APPLICATION_JSON_CHARSET_UTF_8);

        Request request = null;
        try {
            request = requestBuilder.build();
        } catch (Exception e) {
            TimberHelper.e("requestBuilder.build()", e.toString());
        }

        return chain.proceed(request);
    }

}
