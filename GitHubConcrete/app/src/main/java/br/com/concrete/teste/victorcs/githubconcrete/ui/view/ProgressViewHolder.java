package br.com.concrete.teste.victorcs.githubconcrete.ui.view;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import br.com.concrete.teste.victorcs.githubconcrete.R;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Victor Santiago on 21/09/2017.
 */
public class ProgressViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.pbProgress)
    protected ProgressBar pbProgress;

    public ProgressViewHolder(final View itemView) {

        super(itemView);
        ButterKnife.bind(this, itemView);

    }

}