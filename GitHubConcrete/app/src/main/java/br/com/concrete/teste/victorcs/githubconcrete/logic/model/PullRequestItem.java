
package br.com.concrete.teste.victorcs.githubconcrete.logic.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Victor Santiago on 20/09/2017.
 */
public class PullRequestItem implements Parcelable {

    @SerializedName("url")
    private String url;

    @SerializedName("id")
    private Integer id;

    @SerializedName("html_url")
    private String htmlUrl;

    @SerializedName("number")
    private Integer number;

    @SerializedName("state")
    private String state;

    @SerializedName("locked")
    private Boolean locked;

    @SerializedName("title")
    private String title;

    @SerializedName("user")
    private User user;

    @SerializedName("body")
    private String body;

    @SerializedName("created_at")
    private String createdAt;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getHtmlUrl() {
        return htmlUrl;
    }

    public void setHtmlUrl(String htmlUrl) {
        this.htmlUrl = htmlUrl;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Boolean getLocked() {
        return locked;
    }

    public void setLocked(Boolean locked) {
        this.locked = locked;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.url);
        dest.writeValue(this.id);
        dest.writeString(this.htmlUrl);
        dest.writeValue(this.number);
        dest.writeString(this.state);
        dest.writeValue(this.locked);
        dest.writeString(this.title);
        dest.writeParcelable(this.user, flags);
        dest.writeString(this.body);
        dest.writeString(this.createdAt);
    }

    public PullRequestItem() {
    }

    protected PullRequestItem(Parcel in) {
        this.url = in.readString();
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.htmlUrl = in.readString();
        this.number = (Integer) in.readValue(Integer.class.getClassLoader());
        this.state = in.readString();
        this.locked = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.title = in.readString();
        this.user = in.readParcelable(User.class.getClassLoader());
        this.body = in.readString();
        this.createdAt = in.readString();
    }

    public static final Creator<PullRequestItem> CREATOR = new Creator<PullRequestItem>() {
        @Override
        public PullRequestItem createFromParcel(Parcel source) {
            return new PullRequestItem(source);
        }

        @Override
        public PullRequestItem[] newArray(int size) {
            return new PullRequestItem[size];
        }
    };
}
