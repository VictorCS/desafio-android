package br.com.concrete.teste.victorcs.githubconcrete.logic.listener;

import br.com.concrete.teste.victorcs.githubconcrete.logic.model.GitHubRepositoriesResponse;

/**
 * Created by VictorCS on 20/09/2017.
 */
public interface OnGitHubRepositoriesListener {

    void onGitHubRepositoriesSuccess(GitHubRepositoriesResponse response);

    void onGitHubRepositoriesError(String error);

}
