package br.com.concrete.teste.victorcs.githubconcrete.logic.listener;

import java.util.ArrayList;

import br.com.concrete.teste.victorcs.githubconcrete.logic.model.PullRequestItem;

/**
 * Created by VictorCS on 20/09/2017.
 */
public interface OnPullRequestListener {

    void onPullRequestSuccess(ArrayList<PullRequestItem> response);

    void onPullRequestError(String error);

}
