package br.com.concrete.teste.victorcs.githubconcrete.application;

import android.support.multidex.MultiDexApplication;

import br.com.concrete.teste.victorcs.githubconcrete.BuildConfig;
import br.com.concrete.teste.victorcs.githubconcrete.logic.rest.AppApi;
import br.com.concrete.teste.victorcs.githubconcrete.logic.rest.AppServiceFactory;
import br.com.concrete.teste.victorcs.githubconcrete.utils.TimberHelper;
import timber.log.Timber;

/**
 * Created by VictorCS on 20/09/2017.
 */
public class AppApplication extends MultiDexApplication {

    private static AppApplication instance;
    private static AppApi etnaApi;

    public static AppApplication getInstance() {
        return instance;
    }

    public static AppApi getAppApiInstance() {
        return etnaApi;
    }

    @Override
    public void onCreate() {

        super.onCreate();
        instance = this;

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }

        initAppApiService();

    }

    private static void initAppApiService() {

        try {

            etnaApi = AppServiceFactory.createMvpService();

        } catch (Exception e) {

            TimberHelper.e("createMvpService", e.toString());

        }

    }

}
