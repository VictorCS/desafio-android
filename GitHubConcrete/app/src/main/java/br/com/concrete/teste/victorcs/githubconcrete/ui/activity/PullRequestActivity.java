package br.com.concrete.teste.victorcs.githubconcrete.ui.activity;

import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import br.com.concrete.teste.victorcs.githubconcrete.R;
import br.com.concrete.teste.victorcs.githubconcrete.logic.listener.RecyclerItemClickListener;
import br.com.concrete.teste.victorcs.githubconcrete.logic.model.PullRequestItem;
import br.com.concrete.teste.victorcs.githubconcrete.logic.presenter.PullRequestPresenter;
import br.com.concrete.teste.victorcs.githubconcrete.ui.adapter.PullRequestRecyclerAdapter;
import br.com.concrete.teste.victorcs.githubconcrete.utils.AppUtils;
import br.com.concrete.teste.victorcs.githubconcrete.utils.TimberHelper;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by VictorCS on 20/09/2017.
 */
public class PullRequestActivity extends BaseActivity
        implements PullRequestPresenter.IPullRequestView {

    private static boolean isCalledActivity = false;

    @BindView(R.id.toolbar)
    protected Toolbar toolbar;
    @BindView(R.id.rvPullRequest)
    protected RecyclerView rvPullRequest;
    @BindView(R.id.container_main)
    protected RelativeLayout containerMain;
    @BindView(R.id.tvOpened)
    protected TextView tvOpened;
    @BindView(R.id.tvClosed)
    protected TextView tvClosed;
    @BindView(R.id.llValues)
    protected LinearLayout llValues;
    private PullRequestPresenter mPresenter;
    @BindView(R.id.ivEmpty)
    protected ImageView ivEmpty;
    @BindView(R.id.tvBoldEmpty)
    protected TextView tvBoldEmpty;
    @BindView(R.id.llEmptyResult)
    protected LinearLayout llEmptyResult;

    //region Activity life cycle
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pull_request);
        ButterKnife.bind(this);
        isCalledActivity = false;
        new PullRequestPresenter(this, getIntent());

    }

    @Override
    public void onBackPressed() {

        finish();
        overridePendingTransition(R.anim.slide_out_return, R.anim.slide_in_return);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {

        if (menuItem.getItemId() == android.R.id.home) {

            onBackPressed();

        }
        return super.onOptionsItemSelected(menuItem);

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        isCalledActivity = false;
    }

    //endregion

    //region IPullRequestView
    @Override
    public void setupToolbar(String title) {

        toolbar.setTitle(title);
        toolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.white));
        toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        setSupportActionBar(toolbar);

        Drawable drawable = toolbar.getNavigationIcon();
        if(drawable != null) {
            drawable.setColorFilter(ContextCompat.getColor(this, R.color.white), PorterDuff.Mode.SRC_ATOP);
        }

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }

    }

    @Override
    public void setPresenter(PullRequestPresenter presenter) {

        this.mPresenter = presenter;
        this.mPresenter.start();

    }

    @Override
    public void setupValues(List<PullRequestItem> items) {

        llValues.setVisibility(View.VISIBLE);
        int closedNum = 0;
        int openedNum = 0;

        for (PullRequestItem item : items) {

            if (item.getState().equals("open")) {

                openedNum++;

            } else {

                closedNum++;

            }

        }

        tvOpened.setText(getString(R.string.opened, openedNum));
        tvClosed.setText(getString(R.string.closed, closedNum));

    }

    @Override
    public void setupRecyclerView(List<PullRequestItem> items) {

        llEmptyResult.setVisibility(View.GONE);
        rvPullRequest.setVisibility(View.VISIBLE);
        PullRequestRecyclerAdapter adapter = new PullRequestRecyclerAdapter(this, items);
        rvPullRequest.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        rvPullRequest.setAdapter(adapter);
        rvPullRequest.setNestedScrollingEnabled(false);
        rvPullRequest.addOnItemTouchListener(new RecyclerItemClickListener(this,
                rvPullRequest, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                if (!isCalledActivity) {

                    isCalledActivity = true;

                    try {

                        PullRequestItem listItemByPosition = ((PullRequestRecyclerAdapter) rvPullRequest.
                                getAdapter()).getListItemByPosition(position);
                        if (listItemByPosition != null &&
                                !TextUtils.isEmpty(listItemByPosition.getHtmlUrl())) {

                            AppUtils.openUrl(PullRequestActivity.this, listItemByPosition.getHtmlUrl());

                        } else {

                            showActionErrorAlerterMessage();
                            TimberHelper.e("onItemClick", "Some item data is null!");

                        }

                    } catch (Exception e) {

                        showActionErrorAlerterMessage();
                        TimberHelper.e("onItemClick", "Some item data is null!");

                    }

                }

            }

            @Override
            public void onItemLongClick(View view, int position) {

            }
        }));

    }

    @Override
    public void showEmptyLayout() {

        llEmptyResult.setVisibility(View.VISIBLE);
        rvPullRequest.setVisibility(View.GONE);
        llValues.setVisibility(View.GONE);

    }

    @Override
    public void callBrowserActivity() {

    }
    //endregion
}
