package br.com.concrete.teste.victorcs.githubconcrete.utils;

import br.com.concrete.teste.victorcs.githubconcrete.application.AppConstants;
import timber.log.Timber;

/**
 * Created by Victor Santiago on 20/09/2017.
 */
public abstract class TimberHelper {

    public static void v(String identifier, String message) {
        Timber.tag(AppConstants.INFO).v(identifier + ": " + message);
    }

    public static void i(String identifier, String message) {
        Timber.tag(AppConstants.INFO).i(identifier + ": " + message);
    }

    public static void e(String identifier, String message) {
        Timber.tag(AppConstants.ERROR).e(identifier + ": " + message);
    }

}
