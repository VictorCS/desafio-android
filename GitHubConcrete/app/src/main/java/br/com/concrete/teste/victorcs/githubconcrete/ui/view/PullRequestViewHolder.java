package br.com.concrete.teste.victorcs.githubconcrete.ui.view;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import br.com.concrete.teste.victorcs.githubconcrete.R;
import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by Victor Santiago on 20/09/2017.
 */
public class PullRequestViewHolder extends RecyclerView.ViewHolder {

    private Context context;
    @BindView(R.id.tvPullRequestTitle)
    protected TextView tvPullRequestTitle;
    @BindView(R.id.tvPullRequestDescription)
    protected TextView tvPullRequestDescription;
    @BindView(R.id.ivPostProfilePhoto)
    protected CircleImageView ivPostProfilePhoto;
    @BindView(R.id.tvUserUsername)
    protected TextView tvUserUsername;

    public PullRequestViewHolder(final View itemView, Context context) {

        super(itemView);
        ButterKnife.bind(this, itemView);
        this.context = context;

    }

    public void setPullRequestTitle(String name) {

        name = TextUtils.isEmpty(name) ? "" : name;
        tvPullRequestTitle.setText(name);

    }

    public void setPullRequestDescription(String description) {

        description = TextUtils.isEmpty(description) ? "" : description;
        tvPullRequestDescription.setText(description);

    }

    public void setUserPhoto(String url) {

        Glide.with(context).load(url)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .error(ContextCompat.getDrawable(context, R.drawable.ic_default_user))
                .into(ivPostProfilePhoto);

    }

    public void setUsername(String username) {

        username = TextUtils.isEmpty(username) ? "" : username;
        tvUserUsername.setText(username);

    }

}