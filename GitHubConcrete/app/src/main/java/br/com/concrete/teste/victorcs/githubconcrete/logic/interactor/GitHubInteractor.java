package br.com.concrete.teste.victorcs.githubconcrete.logic.interactor;

import java.util.ArrayList;

import br.com.concrete.teste.victorcs.githubconcrete.application.AppApplication;
import br.com.concrete.teste.victorcs.githubconcrete.application.AppConstants;
import br.com.concrete.teste.victorcs.githubconcrete.logic.listener.OnGitHubRepositoriesListener;
import br.com.concrete.teste.victorcs.githubconcrete.logic.listener.OnPullRequestListener;
import br.com.concrete.teste.victorcs.githubconcrete.logic.model.GitHubRepositoriesResponse;
import br.com.concrete.teste.victorcs.githubconcrete.logic.model.PullRequestItem;
import br.com.concrete.teste.victorcs.githubconcrete.logic.rest.AppApi;
import br.com.concrete.teste.victorcs.githubconcrete.utils.TimberHelper;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Victor Santiago on 20/09/2017.
 */
public class GitHubInteractor {

    public void getRepoLists(int pageNum, final OnGitHubRepositoriesListener listener) {

        AppApi appApi = AppApplication.getAppApiInstance();

        appApi.getRepoLists(pageNum)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<GitHubRepositoriesResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        try {
                            listener.onGitHubRepositoriesError(e.getMessage());
                        } catch (Throwable f) {
                            TimberHelper.e(AppConstants.ERROR, f.getMessage());
                        }
                    }

                    @Override
                    public void onNext(GitHubRepositoriesResponse response) {
                        listener.onGitHubRepositoriesSuccess(response);
                    }
                });

    }

    public void getPRFromSelection(String ownerLogin, String itemName, final OnPullRequestListener listener) {

        AppApi appApi = AppApplication.getAppApiInstance();

        appApi.getPRFromSelection(ownerLogin, itemName)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ArrayList<PullRequestItem>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        try {
                            listener.onPullRequestError(e.getMessage());
                        } catch (Throwable f) {
                            TimberHelper.e(AppConstants.ERROR, f.getMessage());
                        }
                    }

                    @Override
                    public void onNext(ArrayList<PullRequestItem> response) {

                        listener.onPullRequestSuccess(response);

                    }
                });

    }

}
