package br.com.concrete.teste.victorcs.githubconcrete.logic.rest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import br.com.concrete.teste.victorcs.githubconcrete.application.AppConstants;
import br.com.concrete.teste.victorcs.githubconcrete.utils.TimberHelper;
import me.jessyan.progressmanager.ProgressManager;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Victor Santiago on 20/09/2017.
 */
public class AppServiceFactory {

    public static AppApi createMvpService() {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
            @Override
            public void log(String message) {
                TimberHelper.i("OkHttp", message);
            }
        });

        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        AppHeaderInterceptor AppInterceptor = new AppHeaderInterceptor();

        OkHttpClient.Builder build = ProgressManager.getInstance().with(new OkHttpClient.Builder());
        build.connectTimeout(45, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .readTimeout(45, TimeUnit.SECONDS)
                .addInterceptor(interceptor)
                .addInterceptor(AppInterceptor);

        Gson gson = new GsonBuilder()
//                .setLenient()
                .create();

        Retrofit.Builder retroBuilder = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .baseUrl(AppConstants.API_GITHUB_URL)
                .client(build.build());

        return retroBuilder.build().create(AppApi.class);

    }

}
