package br.com.concrete.teste.victorcs.githubconcrete.ui.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.tapadoo.alerter.Alerter;

import br.com.concrete.teste.victorcs.githubconcrete.R;
import br.com.concrete.teste.victorcs.githubconcrete.ui.view.IBaseView;
import br.com.concrete.teste.victorcs.githubconcrete.utils.NetworkStatsUtil;
import br.com.concrete.teste.victorcs.githubconcrete.utils.TimberHelper;

/**
 * Created by VictorCS on 20/09/2017.
 */
public class BaseActivity extends AppCompatActivity implements IBaseView {

    private ProgressDialog progressDialog;

    //region Activity Life cycle
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    //endregion

    //region IBaseView
    @Override
    public void showProgress() {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (progressDialog == null) {

                    progressDialog = new ProgressDialog(BaseActivity.this);
                    progressDialog.setMessage(getText(R.string.loading_message));
                    progressDialog.setCancelable(false);

                }

                if (!progressDialog.isShowing()) {

                    progressDialog.show();

                }

            }
        });

    }

    @Override
    public void hideProgress() {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (progressDialog != null && progressDialog.isShowing()) {

                    progressDialog.dismiss();

                }

            }
        });

    }

    @Override
    public void hideSoftKeyboard() {

        if (getCurrentFocus() != null) {

            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);

        }

    }

    @Override
    public void showToastMessage(String message) {

        Toast.makeText(this, message, Toast.LENGTH_LONG).show();

    }

    @Override
    public void showSnackMessage(String message) {

        Snackbar snackbar = Snackbar.make(findViewById(R.id.container_main), message, Snackbar.LENGTH_LONG);
        snackbar.show();

    }

    @Override
    public void showAlerterMessage(String title, String message, int iconResId, int backgroundColor) {

        try {

            if (iconResId != 0) {

                Alerter.create(this)
                        .setTitle(title)
                        .setText(message)
                        .setIcon(iconResId)
                        .setBackgroundColorRes(backgroundColor)
                        .setDuration(3000)
                        .show();

            } else {

                Alerter.create(this)
                        .setTitle(title)
                        .setText(message)
                        .setBackgroundColorRes(backgroundColor)
                        .setDuration(3000)
                        .show();

            }

        } catch (Exception e) {

            TimberHelper.e("BaseActivity", "showAlerterMessage: " + e.toString());

        }

    }

    @Override
    public void showNoConnectionSnackMessage() {

        Snackbar snackbar = Snackbar.make(findViewById(R.id.container_main), getText(R.string.whithout_internet), Snackbar.LENGTH_LONG);
        TextView txtMessage = snackbar.getView().findViewById(android.support.design.R.id.snackbar_text);
        txtMessage.setTextColor(ContextCompat.getColor(this, R.color.orangeApp));
        snackbar.getView().setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));
        snackbar.show();

    }

    @Override
    public void showNoConnectionAlerterMessage() {

        try {

            Alerter.create(this)
                    .setTitle(getString(R.string.warning))
                    .setText(getString(R.string.whithout_internet))
                    .setIcon(R.drawable.ic_warning_white)
                    .setBackgroundColorRes(R.color.colorPrimaryDark)
                    .setDuration(3000)
                    .show();

        } catch (Exception e) {

            TimberHelper.e("BaseActivity", "showNoConnectionAlerterMessage: " + e.toString());

        }

    }

    @Override
    public void showActionErrorAlerterMessage() {

        try {

            Alerter.create(this)
                    .setTitle(getString(R.string.warning))
                    .setText(getString(R.string.action_error))
                    .setIcon(R.drawable.ic_warning_white)
                    .setBackgroundColorRes(R.color.colorPrimaryDark)
                    .setDuration(3000)
                    .show();

        } catch (Exception e) {

            TimberHelper.e("BaseActivity", "showActionErrorAlerterMessage: " + e.toString());

        }

    }

    @Override
    public void showDialogOkMessage(Context context, int idString) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        AlertDialog.Builder builderAlert = builder.setMessage(idString)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        dialog.dismiss();

                    }
                });
        builder.create();
        builderAlert.show();

    }

    @Override
    public boolean isOnline() {
        return NetworkStatsUtil.isConnected(this);
    }
    //endregion
}
