package br.com.concrete.teste.victorcs.githubconcrete.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import br.com.concrete.teste.victorcs.githubconcrete.R;
import br.com.concrete.teste.victorcs.githubconcrete.logic.model.PullRequestItem;
import br.com.concrete.teste.victorcs.githubconcrete.logic.model.User;
import br.com.concrete.teste.victorcs.githubconcrete.ui.view.PullRequestViewHolder;

/**
 * Created by Victor Santiago on 20/09/2017.
 */
public class PullRequestRecyclerAdapter extends RecyclerView.Adapter<PullRequestViewHolder> {

    private final Context context;
    public List<PullRequestItem> items;

    public PullRequestRecyclerAdapter(Context context, List<PullRequestItem> items) {
        this.context = context;
        this.items = items;
    }

    public List<PullRequestItem> getItems() {

        return items;

    }

    public PullRequestItem getListItemByPosition(int index) {

        return items.get(index);

    }

    @Override
    public PullRequestViewHolder onCreateViewHolder(ViewGroup parent,
                                                    int viewType) {

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recyclerview_item_pull_request, parent, false);
        return new PullRequestViewHolder(v, context);

    }


    @Override
    public void onBindViewHolder(final PullRequestViewHolder holder, final int position) {

        PullRequestItem myListItem = items.get(position);
        User user = myListItem.getUser();
        if (user != null) {

            holder.setUserPhoto(user.getAvatarUrl());
            holder.setUsername(user.getLogin());

        }
        holder.setPullRequestTitle(myListItem.getTitle());
        holder.setPullRequestDescription(myListItem.getBody());

        holder.setIsRecyclable(false);

    }

    @Override
    public int getItemCount() {
        if (items == null) {
            return 0;
        }
        return items.size();
    }

}